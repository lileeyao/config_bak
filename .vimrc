set nu
syntax enable
filetype on
"let g:tagbar_ctags_bin='/usr/local/bin/ctags'
"let g:tagbar_width=15
set splitbelow
set splitright
"set winheight=40
"set winminheight=30
set winwidth=50
set winminwidth=40
noremap <C-O> <C-W>k<C-W>_
noremap <C-P> <C-W>j<C-W>_
"autocmd VimEnter * nested :TagbarOpen
"autocmd Vimenter * NERDTree
"let g:NERDTreeWinSize=18
"let g:NERDTreeWinPos = "right"
filetype plugin indent on
set tabstop=2
set shiftwidth=2
set smarttab
set expandtab
let delimitMate_expand_cr=1
set backspace=2
set hlsearch
"set cursorline
set mouse=a
execute pathogen#infect()
set laststatus=2
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_ruby_exec = "/Users/lli/.rbenv/shims/ruby"
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = { "passive_filetypes": [ "ruby" ]}
set autoread
noremap <C-n> :bnext<CR>
noremap <C-p> :bprevious<CR>
let g:gitgutter_diff_args=' master'
